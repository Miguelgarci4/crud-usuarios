package com.security.commons.types;

public enum TipoRespuestaWeb {
    CORRECTA(1, "La acción se ha ejecutado correctamente."),
    ERROR(-1, "Ocurrió un error inesperado con ID {0}");
    
    private final int codigo;    
    private final String mensaje;
    
    private TipoRespuestaWeb(int codigo, String mensaje) {
        this.codigo = codigo;
        this.mensaje = mensaje;
    }

    public int getCodigo() {
        return codigo;
    }

    public String getMensaje() {
        return mensaje;
    }
}