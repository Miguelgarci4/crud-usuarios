package com.security.commons.types;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsersDto {
	private int userId;
	private String userName;
	private String userPassword;
}
