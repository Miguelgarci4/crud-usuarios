package com.security.operation.service;

import java.util.List;

import com.security.commons.types.UsersDto;

public interface UsersService {
	void saveUser(UsersDto user);
	void updateUser(UsersDto user);
	List<UsersDto>listUser();
	<T> T findByUserName(String usuarioCorreo, Class<T> clase) throws Exception;
	void deleteUser(int id);
}
