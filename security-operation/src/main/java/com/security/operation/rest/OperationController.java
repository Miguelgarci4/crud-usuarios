package com.security.operation.rest;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.security.commons.helpers.ExceptionUtil;
import com.security.commons.helpers.RespuestaWeb;
import com.security.commons.types.TipoRespuestaWeb;
import com.security.commons.types.UsersDto;
import com.security.operation.service.UsersService;

@RestController
@RequestMapping("/api/user")
public class OperationController {
	
	Logger LOGGER = LoggerFactory.getLogger(OperationController.class);
	
	@Autowired
	private UsersService usersService;
	
	@RequestMapping(path = "/crear", method = RequestMethod.POST)
	public ResponseEntity<?> crear(@RequestBody UsersDto usersDto) {
		RespuestaWeb respuestaWeb = new RespuestaWeb();
		try {
			usersService.saveUser(usersDto);			
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.CORRECTA);
			return ResponseEntity.ok(respuestaWeb);
		}catch (Exception excepcion) {
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.ERROR);
			LOGGER.error("Error en: OperationController - Metodo: crear:", excepcion);
			return ResponseEntity.badRequest().body(ExceptionUtil.controlar(excepcion));
		}
	}
	
	@RequestMapping(path = "/listar", method = RequestMethod.GET)
	public ResponseEntity<?> listar(){
		RespuestaWeb respuestaWeb = new RespuestaWeb();
		try {
			List<UsersDto> lstUsersDto = new ArrayList<UsersDto>();
			lstUsersDto = usersService.listUser();			
			respuestaWeb.getParametros().put("lstUsersDto", lstUsersDto);
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.CORRECTA);
			return ResponseEntity.ok(respuestaWeb);
		}catch (Exception excepcion) {
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.ERROR);
			LOGGER.error("Error en: OperationController - Metodo: listar:", excepcion);
			return ResponseEntity.badRequest().body(ExceptionUtil.controlar(excepcion));
		}		
	}
	
	@RequestMapping(path = "/actualizar", method = RequestMethod.POST)
	public ResponseEntity<?> actualizar(@RequestBody UsersDto usersDto) {
		RespuestaWeb respuestaWeb = new RespuestaWeb();
		try {
			usersService.updateUser(usersDto);
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.CORRECTA);
			return ResponseEntity.ok(respuestaWeb);
		}catch (Exception excepcion) {
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.ERROR);
			LOGGER.error("Error en: OperationController - Metodo: actualizar:", excepcion);
			return ResponseEntity.badRequest().body(ExceptionUtil.controlar(excepcion));
		}
	}
	
	@RequestMapping(path = "/eliminar/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> eliminar(@PathVariable ("id") int id) {
		RespuestaWeb respuestaWeb = new RespuestaWeb();
		try {
			usersService.deleteUser(id);
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.CORRECTA);
			return ResponseEntity.ok(respuestaWeb);
		}catch (Exception excepcion) {
			respuestaWeb.setTipoRespuesta(TipoRespuestaWeb.ERROR);
			LOGGER.error("Error en: OperationController - Metodo: eliminar:", excepcion);
			return ResponseEntity.badRequest().body(ExceptionUtil.controlar(excepcion));
		}
	}
	
}
