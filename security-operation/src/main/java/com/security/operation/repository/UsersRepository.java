package com.security.operation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.security.operation.model.Users;

@Repository
public interface UsersRepository  extends JpaRepository<Users, Integer>{
	@Query(value = "SELECT * FROM users WHERE user_name = :userName", nativeQuery=true)
	<T> T findByUserName(@Param("userName") String userName, Class<T> clase)throws Exception;
}
