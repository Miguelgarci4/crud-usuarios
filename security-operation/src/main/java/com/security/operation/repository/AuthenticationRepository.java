package com.security.operation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.security.operation.model.Users;

@Repository
public interface AuthenticationRepository extends JpaRepository<Users, Integer>{
	
	@Query(value = "SELECT * FROM USERS WHERE user_name = :userName", nativeQuery = true)
	Users findByUserName(@Param("userName") String userName) throws Exception;
}
