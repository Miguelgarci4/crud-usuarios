package com.security.operation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityOperationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecurityOperationApplication.class, args);
	}

}
