package com.security.operation.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.security.commons.types.UsersDto;
import com.security.operation.model.Users;
import com.security.operation.repository.UsersRepository;
import com.security.operation.service.UsersService;

@Service
public class UsersServiceImpl implements UsersService{

	@Autowired
	private UsersRepository usersRepository;
	
	@Override
	public void saveUser(UsersDto userDto) {
		Users user = new Users();
		user.setUserName(userDto.getUserName());
		user.setUserPassword(userDto.getUserPassword());
		usersRepository.save(user);
	}

	@Override
	public void updateUser(UsersDto userDto) {
		Users user = new Users();
		user.setUserId(userDto.getUserId());
		user.setUserName(userDto.getUserName());
		user.setUserPassword(userDto.getUserPassword());
		usersRepository.saveAndFlush(user);
	}

	@Override
	public List<UsersDto> listUser() {
		List<Users> lstUsers = new ArrayList<Users>();
		List<UsersDto> lstUsersDto = new ArrayList<UsersDto>();				
		lstUsers = usersRepository.findAll();
		lstUsers.forEach(user ->{
			UsersDto userDto = new UsersDto();
			userDto.setUserId(user.getUserId());
			userDto.setUserName(user.getUserName());
			userDto.setUserPassword(user.getUserPassword());
			lstUsersDto.add(userDto);
		});
		return lstUsersDto;
	}
	
	@Override
	public void deleteUser(int id) {
		usersRepository.deleteById(id);
	}

	@Override
	public <T> T findByUserName(String userName, Class<T> clase) throws Exception {		
		return usersRepository.findByUserName(userName, clase);
	}
}
