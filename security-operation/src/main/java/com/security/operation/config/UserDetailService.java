 package com.security.operation.config;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.security.operation.model.Users;
import com.security.operation.repository.AuthenticationRepository;
@Service
public class UserDetailService implements UserDetailsService{
	
	@Autowired
	AuthenticationRepository authenticationRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Users user = new Users(); 
		try {
			user = authenticationRepository.findByUserName(username);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(user != null && user.getUserName().equals(username)) {
			return new User(user.getUserName(), new BCryptPasswordEncoder().encode(user.getUserPassword()), new ArrayList<>());			
		}else
			throw new UsernameNotFoundException("Usuario no existe "+ username);
	}
}
