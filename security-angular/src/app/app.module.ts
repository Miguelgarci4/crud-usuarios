import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './panel/login/login.component';
import { MantenimientoLoginComponent } from './panel/mantenimiento-login/mantenimiento-login.component';
import { DataTablesModule } from "angular-datatables";
import { EditarLoginComponent } from './panel/mantenimiento-login/editar-login/editar-login.component';
import { CrearLoginComponent } from './panel/mantenimiento-login/crear-login/crear-login.component';
import { ModalMensajeComponent } from './panel/modal-mensaje/modal-mensaje.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MantenimientoLoginComponent,
    EditarLoginComponent,
    CrearLoginComponent,
    ModalMensajeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
