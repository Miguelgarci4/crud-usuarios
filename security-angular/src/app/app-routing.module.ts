import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './panel/login/login.component';
import { MantenimientoLoginComponent } from './panel/mantenimiento-login/mantenimiento-login.component';
import { EditarLoginComponent } from './panel/mantenimiento-login/editar-login/editar-login.component';
import { CrearLoginComponent } from './panel/mantenimiento-login/crear-login/crear-login.component';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'editar/:id', component: EditarLoginComponent, canActivate: [AuthGuard] },
  {path: 'mantenimiento', component: MantenimientoLoginComponent, canActivate: [AuthGuard] },
  {path: 'crear', component: CrearLoginComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
