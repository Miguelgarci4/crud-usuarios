import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IUsuario } from 'src/app/models/usuario.model';
import { LoginDataService } from 'src/app/services/login-data.service';

@Component({
  selector: 'app-crear-login',
  templateUrl: './crear-login.component.html',
  styleUrls: ['./crear-login.component.scss']
})
export class CrearLoginComponent implements OnInit {
  public usuarioForm: FormGroup;
  usuario: IUsuario;
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private loginDataService: LoginDataService
  ) { }

  ngOnInit(): void {
    this.usuarioForm = this.formBuilder.group({
      id: ['', [Validators.required, Validators.pattern('/^[1-9]\d*$/')]],
      nombreUsuario: ['', [Validators.required, Validators.pattern('^([a-zA-ZáéíóúÁÉÍÓÚñÑ]+\\s{0,})*$')]],
      contaseniaUsuario: [''],
    })
  }
  guardar() {
    this.usuario = {
      userId: this.usuarioForm.value.id,
      userName: this.usuarioForm.value.nombreUsuario,
      userPassword: this.usuarioForm.value.contaseniaUsuario
    }
    this.loginDataService.crearUsuario(this.usuario).subscribe(data => {
      this.router.navigate(['/mantenimiento']);
    })
  }

  cancelar() {
    this.router.navigate(['/mantenimiento']);
  }
}
