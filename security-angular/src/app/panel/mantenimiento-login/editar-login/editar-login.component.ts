import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { IUsuario } from 'src/app/models/usuario.model';
import { LoginDataService } from 'src/app/services/login-data.service';

@Component({
  selector: 'app-editar-login',
  templateUrl: './editar-login.component.html',
  styleUrls: ['./editar-login.component.scss']
})
export class EditarLoginComponent implements OnInit {
  indice: number;
  usuarios: IUsuario[];
  usuario: IUsuario;
  public usuarioForm: FormGroup;

  constructor(
    private router: Router, 
    private route: ActivatedRoute, 
    private formBuilder: FormBuilder, 
    private loginDataService: LoginDataService
    ) { }

  ngOnInit(): void {
    this.indice = this.route.snapshot.params['id'];
    this.usuarioForm = this.formBuilder.group({
      id: ['', [Validators.required, Validators.pattern('/^[1-9]\d*$/')]],
      nombreUsuario: ['', [Validators.required, Validators.pattern('^([a-zA-ZáéíóúÁÉÍÓÚñÑ]+\\s{0,})*$')]],
      contaseniaUsuario: [''],
    });
    this.obtenerUsuario();
  }

  obtenerUsuario() {
    this.loginDataService.listarUsuarios().subscribe(data => {
      console.log(data)
      this.usuarios = data['parametros'].lstUsersDto;
      this.usuarioForm.get('id').setValue(this.usuarios[this.indice].userId);
      this.usuarioForm.get('nombreUsuario').setValue(this.usuarios[this.indice].userName);
      this.usuarioForm.get('contaseniaUsuario').setValue(this.usuarios[this.indice].userPassword);

    })
  }

  guardar() {
    console.log(this.usuarioForm.value.nombreUsuario);
    this.usuario = {
      userId: this.usuarioForm.value.id,
      userName: this.usuarioForm.value.nombreUsuario,
      userPassword: this.usuarioForm.value.contaseniaUsuario
    }
    this.loginDataService.actualizarUsuario(this.usuario).subscribe(data => {
      this.router.navigate(['/mantenimiento']);
    })
  }

  cancelar() {
    this.router.navigate(['/mantenimiento']);
  }
}
