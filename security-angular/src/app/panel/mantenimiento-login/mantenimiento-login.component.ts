import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { LoginDataService } from 'src/app/services/login-data.service';

@Component({
  selector: 'app-mantenimiento-login',
  templateUrl: './mantenimiento-login.component.html',
  styleUrls: ['./mantenimiento-login.component.scss']
})
export class MantenimientoLoginComponent implements OnInit {

  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;
  showTable: boolean = false;

  ListUsuario: any;

  constructor(private http: HttpClient, private router: Router, private loginDataService: LoginDataService) { }

  ngOnInit(): void {
    this.dtOptions = {
      tableId: "tableId",
      scrollY: "530px",
      autoWidth: true,
      pageLength: 10,
      order: [[0, 'asc']],
      language: {
        "emptyTable": "No hay información disponible",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
        "infoEmpty": "",
        "infoFiltered": "(filtrado de _MAX_ entradas totales)",
        "lengthMenu": "_MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar registro",
        "zeroRecords": "Sin resultados",
        "paginate": {
          "first": "Primera",
          "last": "Última",
          "next": "Siguiente",
          "previous": "Anterior"
        },
        "aria": {
          "sortAscending": ": ordenar ascendentemente",
          "sortDescending": ": ordenar descendientemente"
        }
      },
      searching: true,
      dom: "<'d-flex align-items-center justify-content-between'f><rt><'d-flex align-items-center justify-content-between'lip>",
      responsive: true,
    };
    this.listarUsuarios();
  }

  listarUsuarios() {
    this.loginDataService.listarUsuarios().subscribe( (data:any) =>{
      this.ListUsuario = data['parametros'].lstUsersDto;
      this.dtTrigger.next(data.dtOptions);
      this.showTable = true;
    })
  }

  crearUsuario(){
    this.router.navigate(['/crear']);
  }

  eliminar(posicion : number){
    this.loginDataService.eliminarUsuario(posicion).subscribe(data => {
      this.ngOnDestroy()
      this.listarUsuarios()
    })
  }

  closedStatus: Array<boolean> = [];
  accionOpcion: number;
  toggleDropdown(event: Event, i) {
    event.stopPropagation();
    this.accionOpcion = i;
    let current = this.closedStatus[i];
    this.closedStatus.fill(true)

    if (current == undefined) {
      current = true
    }

    this.closedStatus[i] = current;
    this.closedStatus[i] = !this.closedStatus[i]
  }

  hideDropdown(index: number) {
    this.closedStatus[index] = true;
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  onResize() {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.columns.adjust();
    });
  }
}
