import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { LoginDataService } from 'src/app/services/login-data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public isLoggedIn = new BehaviorSubject<boolean>(false);
  constructor(private loginDataService: LoginDataService, private router: Router) { }
  isVisible = false;
  ngOnInit(): void {
  }

  validarUsuario(form:NgForm){
    const usuario = form.value.txtUsuario;
    const contrasenia = form.value.txtPassword;
    this.loginDataService.obtenerToken(usuario, contrasenia).subscribe
    (
        (responses) => {
            const token = responses['access_token'];
            console.log("Se han guardado los registros :");
            localStorage.setItem('access_token', token);
            this.isLoggedIn.next(true)
            this.router.navigate(['/mantenimiento'])
        },
        error => {
            console.log("Error :" + error)
            this.openPopup()
        }
    );    
  }

  borrarToken(){
    localStorage.removeItem('access_token');
  }

  closePopup() {
    this.isVisible = false;
  }

  openPopup() {
    this.isVisible = true;
  }
}
