import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modal-mensaje',
  templateUrl: './modal-mensaje.component.html',
  styleUrls: ['./modal-mensaje.component.css']
})
export class ModalMensajeComponent implements OnInit {
  isVisible = false;
  constructor() { }

  ngOnInit(): void {
  }
  closePopup() {
    this.isVisible = false;
  }
}
