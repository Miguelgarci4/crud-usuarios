import { Injectable } from '@angular/core';
import { URL_SERVICIOS } from 'src/app/config/config';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from 'rxjs';
import { TokenInterface } from '../models/token-interface';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { IUsuario } from '../models/usuario.model';

@Injectable({
    providedIn: 'root'
})
export class LoginDataService {
    private urlGenerateToken = '/api/token';
    private urlListarUsuarios = '/api/user/listar';
    private urlActualizarUsuario = '/api/user/actualizar';
    private urlCrearUsuario = '/api/user/crear';
    private urlEliminarUsuario = '/api/user/eliminar';
    public isLoggedIn = new BehaviorSubject<boolean>(false);  

    constructor(private router: Router, private httpClient: HttpClient) { 
    }

    obtenerToken(user: any, password: any) {
        const clientIdAndSecret = btoa('clienteId:clienteContrasenia');
        let usuario = user
        let contraseña = password
        const headers = new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
            .set('Authorization', `Basic ${clientIdAndSecret}`)

        const body = new HttpParams()
            .set('grant_type', 'password')
            .set('username', usuario)
            .set('password', contraseña);

        return this.httpClient.post<TokenInterface>(this.urlGenerateToken, body, { headers })
    }

    listarUsuarios(){
        const token = localStorage.getItem('access_token');        
        const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`) 
        return this.httpClient.get(this.urlListarUsuarios,{headers})
    }

    actualizarUsuario(usuario : IUsuario){
        const token = localStorage.getItem('access_token');        
        const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`) 
        return this.httpClient.post<IUsuario>(this.urlActualizarUsuario, usuario, {headers})
    }

    crearUsuario(usuario : IUsuario){
        const token = localStorage.getItem('access_token');        
        const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`) 
        return this.httpClient.post<IUsuario>(this.urlCrearUsuario, usuario, {headers})
    }

    eliminarUsuario(usuarioId : number){
        const token = localStorage.getItem('access_token');        
        const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`) 
        return this.httpClient.delete(this.urlEliminarUsuario + "/" + usuarioId, {headers})
    }
}