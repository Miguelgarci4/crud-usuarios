import { Component } from '@angular/core';
import { LoginDataService } from './services/login-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'security-angular';
  isLogged?: boolean;

  constructor(private loginDataService: LoginDataService, private router: Router) {
    loginDataService.isLoggedIn.subscribe((isLogged: boolean) => {      
    });
    
  }

  ngOnInit(): void {
    this.validarLogged();
  }

  logout() {
    localStorage.removeItem('access_token');
    this.router.navigate(['/login']);
  }

  validarLogged(){
    const esActivo :  boolean = localStorage.getItem('access_token') != null ? true:false;
    this.isLogged = esActivo
  }
}
